# Clustering

Workarround:

Reboot en el momento en el que el nodo que queda decide ya que se tiene que rebotear porque algo no va bien (está colgado, dependiendo del escenario, el nfs-client o el gfs2).

```
#!/bin/sh
HOSTNAME=$(/usr/bin/uname -n)
SEARCH="Operation st_notify_fence requested by $HOSTNAME for peer $HOSTNAME: No such device "
if [ -z "${CRM_notify_desc##$SEARCH*}" ]; then
  echo "OTHER NODE MISSING. REBOOTING TO BE THE MASTER..." >> /var/log/clustermon.log
  /usr/sbin/pcs stonith history cleanup
  /usr/sbin/drbdadm primary storage
  /usr/bin/echo 1 > /proc/sys/kernel/sysrq
  /usr/bin/echo b > /proc/sysrq-trigger
fi
```

Estamos forzando al pacemaker pues no le mola que con dos nodos sólo decidir él solo qué hacer... 
Entonces, supongamos que un nodo se va y no vuelve... 

1. En el nodo que queda vivo pillamos ese mensaje del pcs que detecta que el otro está missing. Podríamos ahí esperar unos minutos, y si el otro no vuelve (pcs sigue diciendo que el nodo está down por los dos canales de corosync que tenemos...) decidimos que nosotros somos los únicos que quedamos vivos 

1.2. Imagina que lo matas (off on) y el otro tarda en arrancar pero arranca, y tu mientras decides que estás solo y arrancas... eso es lo que hemos de evitar... por lo tanto yo proponía un timeout... y si no vuelve entonces le haces un ipmi off, se supone que la orden te devolverá si ha tenido éxito al hacer el off, de esa manera te aseguras que realmente está muerto....
Si esa orden no contesta pues entonces haces el pcs standby...

2. Limpiamos lo que haga falta con pcs cleanup

3. Yo dejaría en algún fichero por ejemplo en /etc/OTHER_NOTHER_MISSING constancia de que hemos rebootado por ese motivo

3.1. Mensaje: echo $(date -u) "I'm alone! Hard rebooting myself to be the master again!" >> /var/log/clustermon.log

3. Como el sistema de ficheros gfs2 está colgado sólo podemos reiniciar, y mejor hacerlo a lo bruto, yo preferiría hacerlo por ipmi contra mi mismo con un cold reset (en las pruebas ya va bien haciendo un echo en sysrq)

4. Reinicia el nodo, y sigue sin detectar al otro, pero si todo el pcs está bien configurado arranca el gfs2 con un solo nodo. 

5. Si posteriormente se arranca el otro nodo que estaba caído, drbd detectaría en su caso un split brain y no dejaría montar, o dependiendo de las políticas que se tengan descartaría sus datos y se pondría a sincronizar...

c) Con GFS2 está el tema del chequeo de disco. Ya vimos que puede quedarse frito el sistema con estos reboots y que estaria bien hacer un fsck.gfs2 cuando pasan estas cosas. Pero hay que ir con cuidado porque el fsck se debe hacer con el sistema de ficheros desmontado de todos los nodos... Si nos aparece el otro mientras hacemos fsck la podemos liar.


Entiendo que el único que podría rearrancar al otro nodo sería en nodo que quedó vivo, por lo que igual molaría llegado a este punto impedirle al pcs que lo arranque por stonith (no se bien como se puede hacer eso, pero seguramente se puede).
Entonces al hacer el reboot podríamos meter un recurso de systemd del que dependan pcs/corosync que mirase ese fichero de /var/log/clustermon.log o otro fichero, y si existe que haga un fscsk. Después de acabar el fcsk lo borra y sigue arrancando el cluster con la seguridad de que está bien chequeado...

Te quedaría el cluster con un nodo arrancado, con el sistema de ficheros verificado y luego manualmente habrías de buscar un momento para hacer mantenimiento del cluster y arreglar el drbd...

Podria el administrador arrancar el otro. Por lo que he comprobado el pacemaker cuando ve que el nodo ha desaparecido le hace un OFF y un ON, y ya no vuelve a intentar arrancarlo jamás.

Entiendo que si cuando vuelve el otro sincroniza el drbd, ya lo haría con el fsck hecho que tiene el otro, es decir, ya tendría el sistema limpio

Es decir, lo que hay que forzar cuando vuelve el otro es una resyncronización del drbd

Para simular que se va del todo apago el nodo via virsh, veo que el pacemaker me lo arranca y lo vuelvo a parar yo. A partir de ahí jamás vuelve a arrancar

https://access.redhat.com/documentation/en-us/red_hat_enterprise_linux/7/html/global_file_system_2/s1-manage-repairfs

Warning
All computers must have the filesystem unmounted before running fsck.gfs2. Failure to unmount from all nodes in a cluster will likely result in filesystem corruption.

Hay que asegurarse es que en todos esté desmonatado. En realidad desde el punto de vista lógico el drbd sólo es uno... lo que pasa que al ser doble primario te has de asegurar que el otro realmente no lo tiene montado.

Hay que hacer que el script haga un pcs node standby del que ha quedado frito. Eso evitará que arranque el pcs. Entonces hay que hacer un fence y asegurar que hago el fsck antes de arrancar.

Workarround: En este caso podria ponerlo todo en standby y tener un cron que cuando arranca si ve que estan los dos en standby haga un fsck y luego los saque los dos de standby. Hay que avisar al administrador.

Hay que haver pruebas de caidas con el script.

```
root@cluster1-cs:~# pcs resource config filesystem-clone
 Clone: filesystem-clone
  Meta Attrs: clone-max=2 clone-node-max=1 interleave=true on-fail=fence
  Resource: filesystem (class=ocf provider=heartbeat type=Filesystem)
   Attributes: device=/dev/drbd1000 directory=/mnt fstype=gfs2 options=defaults,noatime,nodiratime,noquota
   Operations: monitor interval=10s (filesystem-monitor-interval-10s)
               notify interval=0s timeout=20s (filesystem-notify-interval-0s)
               start interval=0s timeout=20s (filesystem-start-interval-0s)
               stop interval=0s timeout=20s (filesystem-stop-interval-0s)
```
To ensure that fsck.gfs2 command does not run on a GFS2 file system at boot time, you can set the run_fsck parameter of the options argument when creating the GFS2 file system resource in a cluster. Specifying "run_fsck=no" will indicate that you should not run the fsck command.

Entiendo que deberia poner lo del run_fsck=no. Ahora no está puesto y quizá eso generó las corrupciones.
