# IsardVDI

## Notes from isardvdi-fronted meeting:

La part d'administració del frontend podria estar feta amb quelcom ja exitent per tal d'agilitzar el desenvolupament i reduir el manteniment.

Cal poder accedir alhora a la part simplificada i la d'adminisració. Cal evaluar si fer dos aplicacions i com resprofitar parts com l'autenticació


## Tecnologies

```text
  Frontend            Backend         Ccontroller
     |------GraphQL------|------GRPC------|
  Vue3 TypeScript      GoLang           GoLang
```

- Database
    - Requirements
        - PostgreSQL
        - ORM
        - Migrations
    - Options:
        - <https://github.com/go-pg/pg>


## Objectius

- Frontend
    - No persistents
    - Persistents
    - Crear plantilla
    - Gestionar permisos (compartició plantilles)
    - Visor
    - Link a accés directe a visor
- Backend
- Permisos
    - Usuaris/Rols/Categories/Grups
    - Plantilles (propietari o compartit amb nosaltres)
    - Escriptoris (propietari o compartit amb nosaltres)
    - Recursos
    - Networks
    - Video
    - Graphics
    - Boots
- Límits
    - Important definir com van les restriccions de dalt cap a baix i de baix cap a dalt. (user->group->category). Quin és el més restrictiu si hi ha diferents valors als nivells.
    - Limits per categories/grups i ususaris
- Hardware
    - cpus/ram/disk_size
- Domains
    - desktops/templates   ** Pensar en altres recursos futurs (robot, webcam, etc...)
    - Quotes per categories/grups i usuaris
        - Ídem de Límits

## Objectius Extres

- Definir granularment a qué té accés un usuari (checkboxes de crear plantilla accés a visor directe, pot afegir GPU, etc...)
- Estaria bé pensar un sistema que fós ampliable fàcilment. Imaginem que ara tenim un nou recurs a Isard que són robots que es poden conectar a màquines. Caldria pensar una interfície que permeti crear la taula del nou recurs i popular widget i permisos nous al frontend.


## Diseñar la base de datos:
- Ir preparando esquemas en base de datos ya realmente
- Quedar un dia

## Proximas reuniones i tareas:
- quedar con Vitto y Melina,
- dar accesos como admin en alguna plataforma
- recull de llibreries
- tener clara la parte de eventos con graphql y
- Proves de frontend - graphql - subscription

## Tools

<https://dbdiagram.io/>

<https://github.com/dbeaver/dbeaver/wiki/Mock-Data-Generation-in-DBeaver>


