## Agile Workflow

<br>

```mermaid
graph TD;
define-epics[Define Epics]-->prod-back
  prod-back[Create product backlog]-->sprint-eval;
  sprint-eval[Sprint Evaluation]-->daily
  daily[Daily standups]-->sprint-retro
  sprint-retro[Sprint Review and Retrospective]-->sprint-eval
  sprint-retro[Sprint Review and Retrospective]-->epic
  epic[Deliver Epic]
```


- Sprint planning [Primer día del sprint - 1/2H]
- Daily standups [Periodicidad - 15Min]
- Sprint Review
- Sprint Retrospective


Tools:
- Taiga: https://taiga.isardvdi.com/
- GitLab: https://gitlab.com/isard/isardvdi


