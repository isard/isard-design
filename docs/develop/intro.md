# Intro

Per desenvolupar tenim tres maneres:

1. Crear un escriptori a develop.isardvdi.com a partir de la plantilla IsardVDI v3
2. Conectar via vpn a la màquina de develop o bé a la de integration
3. Utilitzar docker-compose localment

## Crear un escriptori a develop personal

L'escriptori a develop s'ha de crear de la plantilla **IsardVDI v3**

**Arrencar codium**
´´´
cd isard-dev
codium
´´´

**Comprovar repositori git**
´´´
git remote -v
git remote set-url origin https://github.com/*USERNAME*/isardvdi
´´´

Ja hi ha git-flow instal.lat.

**git-flow feature**

La crea i la retorna al final de/a branca *develop*
´´´
git flow feature start FEATURE_NAME
git flow feature finish FEATURE_NAME
´´´

**git-flow release**

La crea de *develop* i al final fa merge a *master* i *develop* afegint tag:
- merge a master
- crea tag 3.X.Y
- merge a develop
- esborra la branca release

´´´
git flow release start 3.X.Y
git flow release finish 3.X.Y
git push origin develop master --tags
´´´

**git-flow hotfix**

La crea de *master* i la retorna al final a *master* i a *develop*.
- crea tag
- elimina branca hotfix

´´´
git flow hotfix start 3.X.3
git flow hotfix finish 3.X.3
git push origin develop master --tags
´´´

## Treballar contra serveis develop/integration

Hi ha dues màquines:

- develop: 192.168.128.5
- integration: 192.168.128.10

**Accés serveis**
Instal.lar wireguard (apt install wireguard -y)
Descarregar el visor VPN d'una màquina qualsevol nostra arrencada. És un fitxer isard-vpn.conf.
Canviar de isard-vpn.conf les AllowedIPs de 0.0.0.0/0 a 192.168.128/23
Guardar el fitxer isard-vpn.conf a /etc/wireguard/isard-vpn.conf
Iniciar la vpn: sudo wg-quick up isard-vpn

No caldrà tornar a fer això. D'ara en endavant només caldrà fer el wg-quick up/down isard-vpn per tal de conectar-nos o bé desconectar-nos de la vpn.

** serveis disponibles **

ssh: tcp/22
postgresql: tcp/5432
redis: 
backend:


## Treballar en local amb compose

Instal.lar docker i docker-compose (a /sysadm del repo d'Isard hi ha scripts per a debian 10)

Clonar repo v3 i fer un docker-compose up -d
