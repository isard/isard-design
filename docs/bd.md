# Database

- <https://docs.google.com/drawings/d/1w6qIzyETkmzybspHlSF9zeJir3OJqC5mTpXeVJrAw5k/edit>
- <https://my.vertabelo.com/model/NvfKqITUNOIAKPUILHHLZ1pVgi8hskSq>

Table relations with missing fields

![](bd_images/photo_2021-01-09_14-44-16.jpg)
![](bd_images/photo_2021-01-09_14-44-22.jpg)

Hay que tener en cuenta que un dominio (desktop o template) pueda tener un propietario (creator/owner) y que pueda ser compartido entre varios usuarios. Me refiero especialmente al desktop, ya no solo cambiar el propietario sinó que un desktop pueda ser accesible por varios usuarios (por ejemplo el profesor quiere acceder a los desktops de su grupo de alumnos).

Falta:

- Límites (por categorias y grupos):
    - items: desktops, templates, isos
    - hardware: vcpus, memory, disksize
- Cuotas (por usuario. Pueden ser definidas a nivel de grupo o categoria y se aplica de golpe a todos los users que afecta a nivel individual)

