# Task Queue Systems

Each task queues system has a lot of benefits, the major inconvenients to be adopted by IsardVDI are described below.

## bee-queue, exq, kue, node-rethinkdb-job-queue, queue_classic, resque, sidekiq, toniq, worker

No python support.

## rq, huey, dramatiq, kq

No golang support.

## kuyruk

No workers in golang.

## celery

With RabbitMQ result can only be retrieved once, and only by the client that initiated the task.
<https://docs.celeryq.dev/en/stable/userguide/tasks.html?highlight=RPC#rpc-result-backend-rabbitmq-qpid>
We need Redis to get task status from other client.

## faktory

Non-opensource additional features model <https://contribsys.com/faktory/>

No priority inside of queues but "pyfaktory" python client module can assign a weight to each queue. <https://github.com/ghilesmeddour/faktory_worker_python#consumer-worker>

TLS isn't supported by server but supported by "faktory" python client module, so we can use haproxy, or stunnel, in front of server. <https://github.com/cdrx/faktory_worker_python#connection-to-faktory>

## beanstalkd

Doesn't have password protection nor TLS, but it can be done by haproxy or stunnel.
<https://github.com/beanstalkd/beanstalkd/issues/319>

## gearman

Python gearman libraries are archived <https://github.com/josiahmwalton/python3-gearman>, <https://github.com/Yelp/python-gearman>

## machinery

pypi code of pymachiery not published in github <https://github.com/nmcapule/pymachinery/issues/1>
