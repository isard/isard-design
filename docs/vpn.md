# VPN

![](vpn_images/photo_2020-12-31_15-19-26.jpg)

La izquierda es como funciona ahora ya. La idea es que los hypers remotos se conecten como un cliente más. Su config irá similar a la de users en el hyper.
El tema es que voy a hacer que al crear un hyper se configuren ya los datos necesarios y el VPNc (client) se conectará a la bbdd y cogerá de allí los datos para configurarse.
Entiendo que a la derecha tendremos que usar una intrared entre vpnc e hyper que sea diferente a la que ya usamos a la izquierda (192.168.119) y también habrá que poner un dnsmasq diferente para las máquinas al de la izquierda (192.168.128.0/22, que no sale en el gráfico, solo he puesto la ruta dnsmasq para los guests).

No sé si se puede simplificar. La idea es que al arrancar VPNc se conecta a la BBDD y mira su HOSTNAME en la tabla de hypers dónde encontrará la configuración de wireguard cliente que debe usar y se conectará.

El tema es que creo que deberíamos definir al crear el hyper la subred dnsmasq que crearemos en ese hyper y así evitar que haya solapamientos. Eso o por isardvdi.cfg (hay que solventar el tema de mantener diferentes configs).

También definir la intrasubred entre los dockers vpnc e hyperremote.

Bueno, total, que intentaré algo así poniendo variables en isardvdi.cfg:
```
WIREGUARD_SERVER_IP=10.200.200.1/24
WIREGUARD_HYPER_NET=192.168.119.0/24
WIREGUARD_HYPER_GUESTNET=192.168.128.0/22
```

Creo que con estos datos (y alguno más que ya tenemos de antes) podemos configurar todo y las rutas en los wg y en los dnsmasq de hypers.
En el wireguard principal habrá que ir añadiendo rutas a medida que se añadan hypervisores hacia su red WIREGUARD_HJYPER_GUESTNET contra su IP/DNS. Por lo tanto la WIREGUARD_HYPER_GUESTNET habrá que configurarla también en BBDD para que la pille el wireguard principal y así poder poner la ruta. Eso o podemos crear ya un esquema de dnsmasq basado en un número de hyper y tener predefinidas las rutas en el wireguard principal...

A todo esto falta que engine pille las IPs o bien que las preasignemos a partir de la mac en el dnsmasq de cada hyper...
Hay que mirar qué nos sale mejor. Si predefinir en dnsmasq o bien pillar la ip cuando arranca.

Engine no tiene que poner rutas.

Generando un dnsmasq para el hyper. Lo que el engine debe hacer es meter la red wireguard del hyper en los dominios que se arrancan allí.

Hay que mirar el rango definido y ya establecer macs para todo ese rango supongo

Hay que verificar que eso de dar dos Ips por dchp y añadir rutas dinámicamente en cliente funciona en Windows...

Una opción fácil  sería tener una tabla con 4000 entradas de:
Mac - IP - id-domain - online - lease timestamp

Cada vez que arranca un Desktop le meto una Mac distinta y relleno el iddomain y el lease timestamp

necesitamos diferentes redes en la network wireguard de cada hyper

la que puede ser la misma es la intermedia en todos, la 192.168.119.0/30

Cada hyper debería generar una tabla de ip-mac segun el rango de su WG_HYPER_GUESTNET

```
WG_CLIENTS_NET=10.200.200.0/24
## Net between vpn container and isard-hypervisor container only
## Only two hosts needed so you can use a /30 mask always.
## Take care of setting peers inside the defined subnet!
WG_HYPER_NET=192.168.119.0/30
WG_HYPER_NET_WG_PEER=192.168.119.1
WG_HYPER_NET_HYPER_PEER=192.168.119.2
## Hyper guests network.
## When using multiple hypervisors take care to define different
## ranges here for each hypervisor.
WG_HYPER_GUESTNET=192.168.128.0/22
#WIREGUARD_HYPER_GUESTNET=192.168.130.0/22
#WIREGUARD_HYPER_GUESTNET=192.168.132.0/22
#WIREGUARD_HYPER_GUESTNET=192.168.134.0/22
```

Engine asigna Mac en caliente al hyper y ya sabe que IP tendra, con un sistema para saber cuales se han asignado o no.

Con el paquete ipaddress se puede saber la primera ip libre de una lista.

```
wglist = list(r.table('users').pluck('id','vpn').run())
self.clients_reserved_ips=self.clients_reserved_ips+[p['vpn']['wireguard']['Address'] for p in wglist]
```

Y entonces para pillar la primera ip libre:
```
next_ip = str(next(host for host in self.server_net.hosts() if str(host) not in self.clients_reserved_ips))
```

Y eso devuelve la primera libre aunque sea una eliminada de enmedio del rango

Con mapear esa ip que te da eso al offset de la mac de inicio es suficiente


Cuando se crea una nueva máquina esta función te da la ip libre:
```
    def gen_client_ip(self):
        next_ip = str(next(host for host in self.server_net.hosts() if str(host) not in self.clients_reserved_ips))
        self.clients_reserved_ips.append(next_ip)
        return next_ip
```

Y solo hay que modificarla para que te devuelva cuál será su mac a partir de la ip que se le asigna

Tu simplemente te olvidas. Al crear llamas a la función que te da ip/mac y al eliminar no hay que hacer nada porque desaparece ya de la bbdd.

Hay que pasarle a la función los datos configurados en el hyper y ya

```
/ # cat /var/lib/libvirt/dnsmasq/
default.addnhosts    default.hostsfile    shared.conf          virbr0.status        virbr20.status       wireguard.conf
default.conf         shared.addnhosts     shared.hostsfile     virbr1.status        wireguard.addnhosts  wireguard.hostsfile
/ # cat /var/lib/libvirt/dnsmasq/wireguard.hostsfile 
/ # cat /var/lib/libvirt/dnsmasq/wireguard.conf 
##WARNING:  THIS IS AN AUTO-GENERATED FILE. CHANGES TO IT ARE LIKELY TO BE
##OVERWRITTEN AND LOST.  Changes to this configuration should be made using:
##    virsh net-edit wireguard
## or other application using the libvirt API.
##
## dnsmasq conf file created by libvirt
strict-order
pid-file=/var/run/libvirt/network/wireguard.pid
except-interface=lo
bind-dynamic
interface=virbr20
dhcp-range=192.168.128.33,192.168.129.254,255.255.252.0
dhcp-no-override
dhcp-authoritative
dhcp-lease-max=478
dhcp-hostsfile=/var/lib/libvirt/dnsmasq/wireguard.hostsfile
addn-hosts=/var/lib/libvirt/dnsmasq/wireguard.addnhosts
dhcp-option=121,10.200.200.0/24,192.168.128.1
```

En principio se puede jugar con los hosts. Generando esta config con un xml:
```
<network xmlns:dnsmasq='http://libvirt.org/schemas/network/dnsmasq/1.0'>
  <name>wireguard</name>
  <uuid>98552eb2-3e01-4f4d-9d50-4b824f31caff</uuid>
  <bridge name="virbr20"/>
  <forward mode="route" dev="eth1"/>
  <port isolated='yes'/>
  <ip address="192.168.128.1" netmask="255.255.252.0">
    <dhcp>
      <range start="192.168.128.33" end="192.168.129.254"/>
    </dhcp>
  </ip>
       <dnsmasq:options>
        <dnsmasq:option value="dhcp-option=121,10.200.200.0/24,192.168.128.1"/>
      </dnsmasq:options>
</network>
```

No hace falta nada más. Ya pillará de alguno de estos dos ficheros la asignación:
```
dhcp-hostsfile=/var/lib/libvirt/dnsmasq/wireguard.hostsfile
addn-hosts=/var/lib/libvirt/dnsmasq/wireguard.addnhosts
```

```
-H, --addn-hosts=<file>
    Additional hosts file. Read the specified file as well as /etc/hosts. If --no-hosts is given, read only the specified file. This option may be repeated for more than one additional hosts file. If a directory is given, then read all the files contained in that directory. 
--hostsdir=<path>
    Read all the hosts files contained in the directory. New or changed files are read automatically. See --dhcp-hostsdir for details.

Josep Maria Viñolas, [31.12.20 12:04]
--dhcp-hostsfile=<path>
    Read DHCP host information from the specified file. If a directory is given, then read all the files contained in that directory. The file contains information about one host per line. The format of a line is the same as text to the right of '=' in --dhcp-host. The advantage of storing DHCP host information in this file is that it can be changed without re-starting dnsmasq: the file will be re-read when dnsmasq receives SIGHUP.
```


Habría que llamar a la función para coger la primera IP/Mac libre en cada arranque en función del hyper, es decir, llamarla cuando engine ya ha decidido a que hyper va a arrancarse y parsear entonces el XML para ponerle la Mac.
En los que sean servers hay que usar rango fuera de dhcp y asignar hyper y Mac para siempre (no llamar de nuevo a la función, solo al crear o activar server)

Las IPS de los servers y su asociación a Mac se puede gestionar en webapp, Esa ip es en las otras redes y ahí la Mac es fija, no se cambia una vez creado el desktop, no hay que hacer nada.

Engine solo debe llamar a la función para los que no sean servers, con los servers hay que parsear con la ip/mac que ya lleven predefinida desde webapp


Poniendo un campo de 'server' en el dominio que puede no existir o ser False.
Si existe y está activado tendrá:
```
{'ip':1.1.1.1,'mac':'11:22:33:44:55;66'}
```

la mac dependerá del hyper dónde vaya a arrancar porque estará asociada al rango de IPs excepto en los 'servers' que siempre tendrán fijado el hyper y la ip/mac

En cada hyper habremos creado un fichero /var/lib/libvirt/dnsmasq/wireguard.hostsfile con todas las asignaciones a partir del rango

No se puede cambiar el xml justo antes de arrancar usando los hooks de libvirt.
<https://www.redhat.com/archives/libvir-list/2014-April/msg00678.html>
