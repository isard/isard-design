# IsardVDI Transfer System

## Download

```mermaid
sequenceDiagram
    participant User Browser
    participant isard-api
    participant isard-storage
    participant isard-core_worker
    participant isard-transfer
    participant isard-scheduler
    isard-transfer->>isard-api: Register isard-transfer domain for StoragePools
    note over isard-api: Save the domain in the array transfer_domains in the rethinkdb stroage_pool documents
    User Browser->>isard-api: Create Downloadable Storage
    rect
        note over isard-api,isard-core_worker: create_downloadable_storage task
        isard-api->>isard-storage: Create downloadable storage via convert task
        isard-storage->>isard-core_worker: Set storage to downloadable status via update_status task
    end
    User Browser->>isard-api: Get Download URL
    loop if health check fails try another domain if exists
        note over isard-api: Select a domain from rethinkdb storage_pool document
        isard-api->>isard-transfer: Health Check
    end
    note over isard-api: Generate JWT
    isard-api->>User Browser: URL with JWT
    User Browser->>isard-transfer: URL with JWT
    note over isard-transfer: Check JWT signature
    isard-transfer->>User Browser: File
    isard-scheduler->>isard-storage: Remove expired storages with downloadable status via delete task
```

## Upload

```mermaid
sequenceDiagram
    participant User Browser
    participant isard-api
    participant isard-transfer
    isard-transfer->>isard-api: Register isard-transfer domain for StoragePools
    note over isard-api: Save the domain in the array transfer_domains in the rethinkdb stroage_pool documents
    User Browser->>isard-api: Get Upload URL
    loop if health check fails try another domain if exists
        note over isard-api: Select a domain from rethinkdb storage_pool document
        isard-api->>isard-transfer: Health Check
    end
    note over isard-api: Generate JWT
    isard-api->>User Browser: URL and JWT
    loop until all file chunks was sent
        User Browser->>isard-transfer: Post a chunk to URL with JWT in headers
        note over isard-transfer: Check JWT signature and concatenate chunks if all was received
    end
    isard-transfer->>isard-api: Create storage
```
