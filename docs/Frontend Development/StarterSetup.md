# IsardDVI development setup

1. __Visual Studio code__
    - Plugins
        - Vetur
        - Prettier Formatter for Visual Studio Code
        - Graphql For VSCode
        - GitLens
        - Go for Visual Studio Code
        - Draw.io VS Code Integration
        - vscode-proto3
        - ...
        <br />
    - Settings:
    <br />

    ```
    {
    "window.zoomLevel": 1,
    "editor.fontSize": 16,
    "addSettings": "...."
    }
    ```
     <br />
2. __Install docker__

(Ref: digitalocean.com/community/tutorials/how-to-install-and-use-docker-on-ubuntu-20-04)

- ```sudo apt update```
- ```sudo apt install apt-transport-https ca-certificates curl software-properties-common```
- ```curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -```
- ```sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"```
- ```sudo apt update```
- ```apt-cache policy docker-ce```
- ```sudo apt install docker-ce```

- Controlar que esté instalado y arrancado:
    - ```sudo systemctl status docker```

- Add username to docker group:
    - ```sudo usermod -aG docker ${USER}```
    - ```su - ${USER}```
    - ```id -nG```

- (Optional) Add a username not logged in:
    - ```sudo usermod -aG docker username```


**Install Docker Compose**

lorem ipsum
