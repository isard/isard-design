# Contributing workflow
- add remote "upstream" to the isardvdi repository

- Move Task to 'In Progress'

- Update git:

`git fetch --all`

- Create a branch for the Task with the format 'feature/TG-xxx'

`git switch -c feature/TG-150 main/v3-development`

- Commit your changes and push them to your branch in origin/fork 

- Rebase branch from development-v3 main:

`rebase -i upstream/v3-develop`

- join the commits to leave only unique commits per functionality 

- Push changes and create a Merge Request in GitLab
    

```mermaid
graph TB

  SubGraph1 --> SubGraph1Flow
  subgraph "Reviewer"
  SubGraph1Flow(Checkout and review branch)
  SubGraph1Flow -- Request PR Change --> AssignInProgress[Assign In Progress]
  SubGraph1Flow -- Approve --> merge[Merge] --> fetch-and-rebase[Fetch changes and Rebase]
  end

  subgraph "Contributor"
  in-progress[Task to 'In Progress'] --> create-branch[Create branch feature/TG-xxx]
  create-branch --> do-changes[Make changes]
  do-changes --> commit[Commit and push]
  commit --> SubGraph1[Squash commits and create PR]

  SubGraph1 --> final[Fetch main]
end
```


Main git commands:
- config git editor: `git config core.editor gedit`
- git log --graph --all
- git reflog
- Git rebase  [from branch] [last exscluded commit] [to branch or commit]

` git rebase --onto main/v3-ui-poc 541a394c274ff497df6c1a02b353b2a159c6df78~1 541a394c274ff497df6c1a02b353b2a159c6df78`
