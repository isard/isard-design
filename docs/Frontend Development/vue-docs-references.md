## Links de referencia a documentción Vue3

**Documentación oficial**

- Reactividad:  https://v3.vuejs.org/guide/reactivity-fundamentals.html#destructuring-reactive-state

- Computed y watch: https://v3.vuejs.org/guide/reactivity-computed-watchers.html#computed-values

- Lifecycle hooks: https://v3.vuejs.org/guide/composition-api-lifecycle-hooks.html
