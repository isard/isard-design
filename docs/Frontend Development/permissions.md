# Granular Permissions


| Permissions | administrator | manager | user |
| ------ | ------ | ------ | ------ | 
| can_view_menu_templates | x | x | x |
| can_view_menu_templates | x | x | x |
| can_view_menu_desktops | x | x | x |
| can_view_menu_entities | x ||| 
| can_view_menu_users | x | x || 
| can_view_menu_media | x | x || 
| can_view_menu_hypervisors | x || x || 
| can_view_menu_graphs | x | x || 
| can_view_menu_updates | x | x || 
| can_view_menu_config | x | x || 
| can_view_menu_groups | x | x || 
| - | - | - | - |
| can_create_group | x | x || 
| can_edit_group | x | x || 
| can_delete_group | x | x || 
| - | - | - | - |
| can_upload_iso | x | x || 
| can_share_iso | x | x || 
| can_edit_iso | x | x || 
| can_delete_iso | x | x || 
| - | - | - | - |
| can_create_user | x | x || 
| can_edit_user | x | x || 
| can_delete_user | x | x || 
| - | - | - | - |
| can_create_entity | x | x || 
| can_edit_entity | x | x || 
| can_delete_entity | x | x || 
| - | - | - | - |
| can_edit_template | x | x | x |
| can_create_template | x | x | x |
| can_delete_template | x | x | x |
| - | - | - | - |
| can_edit_desktop | x | x | x |
| can_create_desktop | x | x | x |
| can_delete_desktop | x | x | x |
| - | - | - | - |
| can_edit_template_quotas | x | x || 
| can_modify_template_xml | x | x || 
