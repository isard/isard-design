## Config maquina desarrollo en remoto


- Entar a https://develop.isardvdi.com/

- Crear un desktop a partir de la plantilla "Template isard-dev v-[xx]"

- Esperar proceso instalación, aceptar plugins chrome y cerrar el browser

- configurar el usuario, el upstream en git, los nombres de las ramas master/dev y las de default "por ej feature/v3/"

- clonar el repositorio desde el propio fork en la carpeta workspace

- en la consola arrancar el visual studio ejecutando el comando `codium`


Para ejecutar el cli de sin instalarlo:
`npx @vue/cli create xxx`

Configuración en local:
- poner fichero de configuracion de wireguard en /etc/wireguard/
- La máquina se llama Development v4 y tiene la ip 192.168.129.125


```
apt install wireguard


wg-quick up vitto
wg-quick down vitto
```



