# Vuex store structure

[View latest in Json editor](https://jsoneditoronline.org/#left=cloud.e8feec9d1dad48d5a159fb4fe3a0325e)

```
{
  "auth": {
    "user": {
      "userName": "vtogliatti",
      "email": "lamail@mail,com",
      "detail": {
        "name": "",
        "firstSurname": "",
        "secondSurname": "",
        "id": "",
        "status": "",
        "organizationId": "",
        "roles": "",
        "lastAttempt": "",
        "creationDate": "",
        "entities": [
          "",
          "",
          ""
        ],
        "layout": "manager"
      }
    },
    "token": "fdgfghjhgmhhjkjhgkmmgfhm",
    "loggedIn": true,
    "profile": "admin",
    "groups": [
      "admins",
      "teachers"
    ]
  },
  "router": {
    "state": {
      "url": "templates/search",
      "params": {},
      "queryParams": {},
      "layout": "manager",
      "section": "templates",
      "view": "search",
      "tab": "notes",
      "paths": [
        "templates",
        "search"
      ]
    },
    "uid": "fdsgfdg5y54y"
  },
  "interface": {
    "navOpen": true,
    "editMode": true,
    "isLoading": false,
    "snackBar": {
      "visible": false,
      "configuration": "jsonconfig?"
    },
    "progressBar": {
      "routerLoading": false,
      "datLoading": false,
      "processed": 50
    },
    "dialogStatus": {
      "isOpen": false
    }
  },
  "search": {
    "ids": [
      "1",
      "2"
    ],
    "items": [
      {},
      {}
    ],
    "filterCache": {
      "desktops": {
        "searchFilters": {}
      }
    },
    "input": {
      "term": "freetextpattern",
      "size": 35,
      "from": 70,
      "sort": "attributes.name"
    },
    "extra": {
      "total": 480
    },
    "selectedItem": "",
    "forceCancel": false
  },
  "data": {
    "desktops": {
      "id": "1234",
      "attributes": {
        "name": "Desktop Name",
        "os": "Debian"
      },
      "users": [{
          "id": 1234,
          "attributes": {
            "name": "Neus",
            "firstSurname": "Ribes",
            "secondSurname": "Freser",
            "mail": "nribes@test.com"
          }
        },
        {
          "id": 2222,
          "attributes": {
            "name": "Marti",
            "firstSurname": "first",
            "secondSurname": "second",
            "mail": "mfirst@test.com"
          }
        }],
      "owner": {
        "id": 1111,
          "attributes": {
            "name": "Joan",
            "firstSurname": "Roca",
            "secondSurname": "Massip",
            "mail": "jroca@test.com"
          }
        }
      },
      "users": {
        "id": 1111,
          "attributes": {
            "name": "Joan",
            "firstSurname": "Roca",
            "secondSurname": "Massip",
            "mail": "jroca@test.com"
          }
        },
        "templates": {
        "id": 5454,
          "attributes": {
            "name": "Ubuntu 20 template",
            "firstSurname": "Roca",
            "secondSurname": "Massip",
            "mail": "jroca@test.com"
          },
          "owner": {
            "id": 1111,
              "attributes": {
                "name": "Joan",
                "firstSurname": "Roca",
                "secondSurname": "Massip",
                "mail": "jroca@test.com"
              }
            },
          "groups": [
            {
            "id": 6767,
              "attributes": {
                "name": "tercero A"
              }
            }
            ]
        }
    }
  }
```
