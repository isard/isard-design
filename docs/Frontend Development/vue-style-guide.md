## Estilos para desarrollo Vue / Typescript


Utilizar donde posible:


- Operadores ternarios:

`const showDebug = environment.development ? true : false`


- Asignación de variables con mismo nombre ES6:


```
getUser(name: string, age: number) {
        const newAge = age + 1;
    return {
        age: newAge,
        name
    });
```

